<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_List Songs</name>
   <tag></tag>
   <elementGuidId>d37cef66-f56b-4b80-bfa3-0cb2c1c120c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.yt-simple-endpoint.style-scope.yt-formatted-string</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = 'playlist?list=RDCLAK5uy_kPUiaSzaFOnCS27ZdTxc0OovssjN5Xuv8' and (text() = 'Relaxing Pop Songs' or . = 'Relaxing Pop Songs')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;items&quot;]/ytmusic-two-row-item-renderer[1]/div[1]/div/yt-formatted-string/a
//*[@id=&quot;items&quot;]/ytmusic-navigation-button-renderer[1]/button/yt-formatted-string
//*[@id=&quot;items&quot;]/ytmusic-navigation-button-renderer[5]/button	</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>yt-simple-endpoint style-scope yt-formatted-string</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>playlist?list=RDCLAK5uy_kPUiaSzaFOnCS27ZdTxc0OovssjN5Xuv8</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>auto</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Relaxing Pop Songs</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;browse-page&quot;)/div[@class=&quot;background-gradient style-scope ytmusic-browse-response&quot;]/ytmusic-section-list-renderer[@class=&quot;style-scope ytmusic-browse-response&quot;]/div[@id=&quot;contents&quot;]/ytmusic-grid-renderer[@class=&quot;style-scope ytmusic-section-list-renderer&quot;]/div[@id=&quot;items&quot;]/ytmusic-two-row-item-renderer[@class=&quot;style-scope ytmusic-grid-renderer&quot;]/div[@class=&quot;details style-scope ytmusic-two-row-item-renderer&quot;]/div[@class=&quot;title-group style-scope ytmusic-two-row-item-renderer&quot;]/yt-formatted-string[@class=&quot;title style-scope ytmusic-two-row-item-renderer&quot;]/a[@class=&quot;yt-simple-endpoint style-scope yt-formatted-string&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='items']/ytmusic-two-row-item-renderer/div/div/yt-formatted-string/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Relaxing Pop Songs')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spotlight'])[1]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chill'])[1]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Norah Jones'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=','])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Relaxing Pop Songs']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'playlist?list=RDCLAK5uy_kPUiaSzaFOnCS27ZdTxc0OovssjN5Xuv8')])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//yt-formatted-string/a</value>
   </webElementXpaths>
</WebElementEntity>
