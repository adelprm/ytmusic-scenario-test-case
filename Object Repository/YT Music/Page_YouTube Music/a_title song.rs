<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_title song</name>
   <tag></tag>
   <elementGuidId>44b09408-d869-4259-8467-78a878056346</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>yt-formatted-string.title.style-scope.ytmusic-responsive-list-item-renderer.complex-string > a.yt-simple-endpoint.style-scope.yt-formatted-string</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = 'watch?v=xRX_JZapgX0&amp;list=OLAK5uy_nZpWWiWZdvmCIxngHiQAIV4Si2ZmayJuM' and (text() = 'Left and Right (feat. Jung Kook of BTS)' or . = 'Left and Right (feat. Jung Kook of BTS)')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='contents']/ytmusic-responsive-list-item-renderer/div[2]/div/yt-formatted-string/a
//*[@id=&quot;items&quot;]/ytmusic-navigation-button-renderer[1]/button
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>yt-simple-endpoint style-scope yt-formatted-string</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>watch?v=xRX_JZapgX0&amp;list=OLAK5uy_nZpWWiWZdvmCIxngHiQAIV4Si2ZmayJuM</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Left and Right (feat. Jung Kook of BTS)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;browse-page&quot;)/div[@class=&quot;background-gradient style-scope ytmusic-browse-response&quot;]/ytmusic-section-list-renderer[@class=&quot;style-scope ytmusic-browse-response&quot;]/div[@id=&quot;contents&quot;]/ytmusic-shelf-renderer[@class=&quot;style-scope ytmusic-section-list-renderer&quot;]/div[@id=&quot;contents&quot;]/ytmusic-responsive-list-item-renderer[@class=&quot;style-scope ytmusic-shelf-renderer&quot;]/div[@class=&quot;flex-columns style-scope ytmusic-responsive-list-item-renderer&quot;]/div[@class=&quot;title-column style-scope ytmusic-responsive-list-item-renderer&quot;]/yt-formatted-string[@class=&quot;title style-scope ytmusic-responsive-list-item-renderer complex-string&quot;]/a[@class=&quot;yt-simple-endpoint style-scope yt-formatted-string&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contents']/ytmusic-responsive-list-item-renderer/div[2]/div/yt-formatted-string/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Left and Right (feat. Jung Kook of BTS)')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Songs'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscribe'])[2]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Charlie Puth'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=','])[1]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Left and Right (feat. Jung Kook of BTS)']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'watch?v=xRX_JZapgX0&amp;list=OLAK5uy_nZpWWiWZdvmCIxngHiQAIV4Si2ZmayJuM')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/yt-formatted-string/a</value>
   </webElementXpaths>
</WebElementEntity>
