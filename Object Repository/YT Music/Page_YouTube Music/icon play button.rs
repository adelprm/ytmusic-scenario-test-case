<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon play button</name>
   <tag></tag>
   <elementGuidId>20cd3b62-536e-465f-bbf9-96777620b0dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//ytmusic-play-button-renderer[@id='play-button']/div/yt-icon)[18]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ytmusic-section-list-renderer.style-scope.ytmusic-tabbed-search-results-renderer > #contents > ytmusic-shelf-renderer.style-scope.ytmusic-section-list-renderer > #contents > ytmusic-responsive-list-item-renderer.style-scope.ytmusic-shelf-renderer > div.left-items.style-scope.ytmusic-responsive-list-item-renderer > ytmusic-item-thumbnail-overlay-renderer.thumbnail-overlay.style-scope.ytmusic-responsive-list-item-renderer > #content > #play-button > div.content-wrapper.style-scope.ytmusic-play-button-renderer > yt-icon.icon.style-scope.ytmusic-play-button-renderer</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>yt-icon</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>icon style-scope ytmusic-play-button-renderer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;search-page&quot;)/ytmusic-tabbed-search-results-renderer[@class=&quot;style-scope ytmusic-search-page&quot;]/div[@class=&quot;content style-scope ytmusic-tabbed-search-results-renderer&quot;]/ytmusic-section-list-renderer[@class=&quot;style-scope ytmusic-tabbed-search-results-renderer&quot;]/div[@id=&quot;contents&quot;]/ytmusic-shelf-renderer[@class=&quot;style-scope ytmusic-section-list-renderer&quot;]/div[@id=&quot;contents&quot;]/ytmusic-responsive-list-item-renderer[@class=&quot;style-scope ytmusic-shelf-renderer&quot;]/div[@class=&quot;left-items style-scope ytmusic-responsive-list-item-renderer&quot;]/ytmusic-item-thumbnail-overlay-renderer[@class=&quot;thumbnail-overlay style-scope ytmusic-responsive-list-item-renderer&quot;]/div[@id=&quot;content&quot;]/ytmusic-play-button-renderer[@id=&quot;play-button&quot;]/div[@class=&quot;content-wrapper style-scope ytmusic-play-button-renderer&quot;]/yt-icon[@class=&quot;icon style-scope ytmusic-play-button-renderer&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//ytmusic-play-button-renderer[@id='play-button']/div/yt-icon)[18]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Songs'])[3]/following::yt-icon[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='•'])[53]/following::yt-icon[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='That That (prod. &amp; feat. SUGA of BTS) (feat. SUGA of BTS)'])[1]/preceding::yt-icon[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Song'])[3]/preceding::yt-icon[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ytmusic-shelf-renderer[2]/div[3]/ytmusic-responsive-list-item-renderer/div/ytmusic-item-thumbnail-overlay-renderer/div/ytmusic-play-button-renderer/div/yt-icon</value>
   </webElementXpaths>
</WebElementEntity>
