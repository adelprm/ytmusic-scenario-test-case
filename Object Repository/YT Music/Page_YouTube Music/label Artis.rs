<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label Artis</name>
   <tag></tag>
   <elementGuidId>9c6fcf96-84b1-4518-98aa-52fa3e0ceb52</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;contents&quot;]/ytmusic-responsive-list-item-renderer/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'yt-simple-endpoint style-scope ytmusic-responsive-list-item-renderer' and @href = 'channel/UC9vrvNSL3xcWGSkV86REBSg']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ytmusic-responsive-list-item-renderer.style-scope.ytmusic-shelf-renderer > a.yt-simple-endpoint.style-scope.ytmusic-responsive-list-item-renderer</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>yt-simple-endpoint style-scope ytmusic-responsive-list-item-renderer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>channel/UC9vrvNSL3xcWGSkV86REBSg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>BTS</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;search-page&quot;)/ytmusic-tabbed-search-results-renderer[@class=&quot;style-scope ytmusic-search-page&quot;]/div[@class=&quot;content style-scope ytmusic-tabbed-search-results-renderer&quot;]/ytmusic-section-list-renderer[@class=&quot;style-scope ytmusic-tabbed-search-results-renderer&quot;]/div[@id=&quot;contents&quot;]/ytmusic-shelf-renderer[@class=&quot;style-scope ytmusic-section-list-renderer&quot;]/div[@id=&quot;contents&quot;]/ytmusic-responsive-list-item-renderer[@class=&quot;style-scope ytmusic-shelf-renderer&quot;]/a[@class=&quot;yt-simple-endpoint style-scope ytmusic-responsive-list-item-renderer&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contents']/ytmusic-responsive-list-item-renderer/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Top result'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Artists'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BTS'])[4]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Artist'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'channel/UC9vrvNSL3xcWGSkV86REBSg')])[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/ytmusic-responsive-list-item-renderer/a</value>
   </webElementXpaths>
</WebElementEntity>
