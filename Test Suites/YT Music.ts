<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>YT Music</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8097cc37-041f-4216-9ea2-16c425ee2dd7</testSuiteGuid>
   <testCaseLink>
      <guid>afee8685-3835-45d5-aa58-6b2e20f3d487</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/YT Music/Page Explore/TC Explore</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a8f34055-3414-4097-88f6-de336e1acfb3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/YT Music/DF YT music</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>a8f34055-3414-4097-88f6-de336e1acfb3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Menu</value>
         <variableId>18c88770-edfc-4adc-8065-2dcc7c35fa04</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a8f34055-3414-4097-88f6-de336e1acfb3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>InputYT</value>
         <variableId>f8574f52-e2c7-4649-95e6-a738bde72a6d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
